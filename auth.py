# ⁻*- coding: utf-8 -*-
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License version 3 for
# more details.
#
# You should have received a copy of the GNU General Public License version 3
# along with this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# (c) 2015 Valentin Samir
"""Some authentication classes for the CAS"""
from django.conf import settings
import MySQLdb
import MySQLdb.cursors
import crypt

import os
import base64
import collections

from cas_attrs import models

from cas_server.auth import DummyAuthUser


class MysqlAuthUser(DummyAuthUser):
    """A mysql auth class: authentication user agains a mysql database"""
    user = None
    user_attrs = None
    additional_attrs = None

    def __init__(self, username):
        self.mysql_config = {
            "user": settings.CAS_SQL_USERNAME,
            "passwd": settings.CAS_SQL_PASSWORD,
            "db": settings.CAS_SQL_DBNAME,
            "host": settings.CAS_SQL_HOST,
            "charset": settings.CAS_SQL_DBCHARSET,
            "cursorclass": MySQLdb.cursors.DictCursor
        }
        if not MySQLdb:
            raise RuntimeError("Please install MySQLdb before using the MysqlAuthUser backend")
        conn = MySQLdb.connect(**self.mysql_config)
        curs = conn.cursor()
        if curs.execute(settings.CAS_SQL_USER_QUERY, (username,)) == 1:
            self.user = curs.fetchone()
            try:
                self.user_attrs = models.User.objects.get(username=username).attributes
            except models.User.DoesNotExist:
                user_attrs = models.User.objects.create(username=username)
                for elt in models.AttributeDefault.objects.all():
                    user_attrs.attributes_manager.create(key=elt.key, value=elt.value, default=True)
                self.user_attrs = user_attrs.attributes

            additional_attrs = collections.defaultdict(list)
            for sql in settings.CAS_SQL_ADDITIONAL_ATTRS:
                curs.execute(sql, self.user)
                for row in curs:
                    for key, value in row.items():
                        additional_attrs[key].append(value)

            self.additional_attrs = dict(additional_attrs)

            super(MysqlAuthUser, self).__init__(self.user['username'])

    def test_password(self, password):
        """test `password` agains the user"""
        status = False
        if not self.user:
            return status
        else:
            if settings.CAS_SQL_PASSWORD_CHECK == "plain":
                status = password == self.user["password"]
            elif settings.CAS_SQL_PASSWORD_CHECK == "crypt":
                if self.user["password"].startswith('$'):
                    salt = '$'.join(self.user["password"].split('$', 3)[:-1])
                    status = crypt.crypt(password, salt) == self.user["password"]
                else:
                    status = crypt.crypt(
                        password,
                        self.user["password"][:2]
                    ) == self.user["password"]
        if status is True:
            conn = MySQLdb.connect(**self.mysql_config)
            curs = conn.cursor()
            if settings.CAS_SQL_PASSWORD_CHECK == "crypt" and not password.startswith("$6$"):
                salt = "$6$%s" % base64.b64encode(os.urandom(12))
                new_hashed_password = crypt.crypt(password, salt)
                curs.execute(
                    "UPDATE mailbox SET password = %s WHERE username = %s",
                    (new_hashed_password, self.user['username'])
                )
            updated = set()
            attrs = self.attributs()
            attrs['clear_password'] = password
            for sql in settings.CAS_SQL_UPDATE:
                if curs.execute(sql, attrs) > 0:
                    updated.add(self.user['username'])
            if updated:
                template = ("%s = %%s" % settings.CAS_SQL_ON_UPDATE[1],) * len(updated)
                curs.execute(settings.CAS_SQL_ON_UPDATE[0] % " OR ".join(template), tuple(updated))
            conn.commit()
        return status

    def attributs(self):
        """return a dict of user attributes"""
        if not self.user:
            return {}
        else:
            attrs = {}
            if self.user_attrs:
                attrs.update(self.user_attrs)
            if self.additional_attrs:
                attrs.update(self.additional_attrs)
            attrs.update(self.user)
            return attrs
