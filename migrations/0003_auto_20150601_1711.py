# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cas_attrs', '0002_auto_20150601_1657'),
    ]

    operations = [
        migrations.CreateModel(
            name='AttributeDefault',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(unique=True, max_length=255)),
                ('key', models.ForeignKey(related_name='+', to='cas_attrs.AttributeName', on_delete=models.CASCADE)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='attributedefault',
            unique_together=set([('key', 'value')]),
        ),
        migrations.AlterField(
            model_name='attribute',
            name='user',
            field=models.ForeignKey(related_name='attributes_manager', to='cas_attrs.User', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
