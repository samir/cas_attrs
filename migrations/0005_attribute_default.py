# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cas_attrs', '0004_auto_20150601_1744'),
    ]

    operations = [
        migrations.AddField(
            model_name='attribute',
            name='default',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
