# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cas_attrs', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='attribute',
            old_name='name',
            new_name='key',
        ),
        migrations.AlterUniqueTogether(
            name='attribute',
            unique_together=set([('user', 'key', 'value')]),
        ),
    ]
