# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cas_attrs', '0003_auto_20150601_1711'),
    ]

    operations = [
        migrations.AlterField(
            model_name='attribute',
            name='value',
            field=models.CharField(max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='attributedefault',
            name='value',
            field=models.CharField(max_length=255),
            preserve_default=True,
        ),
    ]
