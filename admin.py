from django.contrib import admin
from cas_attrs import models
# Register your models here.
#class User(models.Model):
#class AttributeName(models):
#class Attribute(models):

class AttributeInline(admin.TabularInline):
    model = models.Attribute
    extra = 0

class UserAdmin(admin.ModelAdmin):
    """`User` in admin interface"""
    inlines = [AttributeInline]

class AttributeDefaultAdmin(admin.ModelAdmin):
    """`ServicePattern` in admin interface"""
    list_display = ('key', 'value')

admin.site.register(models.User, UserAdmin)
admin.site.register(models.AttributeName, admin.ModelAdmin)
admin.site.register(models.AttributeDefault, AttributeDefaultAdmin)
