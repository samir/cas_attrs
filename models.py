# ⁻*- coding: utf-8 -*-
from django.db import models, IntegrityError
import collections
# Create your models here.

class User(models.Model):
    username = models.CharField(max_length=30, unique=True)
    def __unicode__(self):
        return self.username

    @property
    def attributes(self):
        attrs = collections.defaultdict(list)
        for elt in self.attributes_manager.all():
            attrs[elt.key.value].append(elt.value)
        return dict(attrs)

class AttributeName(models.Model):
    value = models.CharField(max_length=255, unique=True)
    def __unicode__(self):
        return self.value

class Attribute(models.Model):
    class Meta:
        unique_together = ('user', 'key', 'value')

    user = models.ForeignKey(User, related_name="attributes_manager", on_delete=models.CASCADE)
    key = models.ForeignKey(AttributeName, related_name="attributes", on_delete=models.CASCADE)
    value = models.CharField(max_length=255)
    default = models.BooleanField(default=False)

    def save(self, *args, **kwarg):
        if self.pk:
            self.default = False
        return super(Attribute, self).save(*args, **kwarg)

    def __unicode__(self):
        return u"%s(%s → %s)%s" % (self.user, self.key, self.value, ' default' if self.default else '')


class AttributeDefault(models.Model):
    class Meta:
        unique_together = ('key', 'value')
    key = models.ForeignKey(AttributeName, related_name="+", on_delete=models.CASCADE)
    value = models.CharField(max_length=255)


    def __init__(self, *args, **kwargs):
        super(AttributeDefault, self).__init__(*args, **kwargs)
        if self.pk:
            self.old_field = {'key':self.key, 'value':self.value}
        else:
            self.old_field = {}

    def populate(self):
        for user in User.objects.all():
            try:
                user.attributes_manager.get(key=self.key, value=self.value)
            except Attribute.DoesNotExist:
                attr = user.attributes_manager.create(key=self.key, value=self.value, default=True)

    def delete(self, *args, **kwarg):
        Attribute.objects.filter(key=self.key, value=self.value, default=True).delete()
        super(AttributeDefault, self).delete(*args, **kwarg)

    def save(self, *args, **kwarg):
        update = True if self.pk else False
        super(AttributeDefault, self).save(*args, **kwarg)
        if update:
            Attribute.objects.filter(key=self.old_field['key'], value=self.old_field['value'], default=True).update(key=self.key, value=self.value, default=True)
        else:
            self.populate()
